# SWORD-Examples

Einige Beispiele in Python zur Konvertierung von Quellen.


## Karl Barth: Kurze Auslegung des Römerbriefs

Dieses Werk steht auf <a rel="noreferrer noopener" href="https://archive.org/details/20200323kurzeerklarungdesrdmerbriefes_202003/mode/2up" data-type="URL" data-id="https://archive.org/details/20200323kurzeerklarungdesrdmerbriefes_202003/mode/2up" target="_blank">archive.org</a> zur Verfügung und ist als Public Domain gekennzeichnet. Da das US-amerikanische PD nicht mit dem europäischen „gemeinfrei“ übereinstimmt, ist die Rechtslage in Deutschland nicht ganz so einfach. Technisch steht aber ein OCR-Text in verhältnismäßig guter Qualität zur Verfügung ("<a href="https://archive.org/stream/20200323kurzeerklarungdesrdmerbriefes_202003/20200323_KURZE%20ERKLARUNG%20DES%20RDMERBRIEFES_djvu.txt">FULL TEXT</a>" im rechten Kasten). 

Als Grundlage können wir - wie im vorherigen Teil - folgendes Grundgerüst nehmen:


<pre class="wp-block-code"><code>&lt;?xml version="1.0" encoding="UTF-8" ?&gt;
&lt;osis xmlns="http://www.bibletechnologies.net/2003/OSIS/namespace" 
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.bibletechnologies.net/2003/OSIS/namespace
      http://www.bibletechnologies.net/osisCore.2.1.1.xsd"&gt;
      
&lt;osisText osisIDWork="Barth-Röm" osisRefWork="book" xml:lang="de"&gt;
    &lt;header&gt;

      &lt;work osisWork="WorkID"&gt;
      &lt;title&gt;Kurze Erklärung des Römerbriefs&lt;/title&gt;
      &lt;creator role="aut"&gt;Karl Barth&lt;/creator&gt;
      &lt;/work&gt;
      &lt;work osisWork="Bible"&gt;
      &lt;refSystem&gt;Bible&lt;/refSystem&gt;
      &lt;/work&gt;
    &lt;/header&gt;
    
    
&lt;div type="book" osisID="Kurze Erklärung des Römerbriefs"&gt;
  &#91;...]
&lt;/div&gt;
&lt;/osisText&gt;
&lt;/osis&gt;</code></pre>
<!-- /wp:code -->

<!-- wp:paragraph -->
<p>Der obere Bereich beinhaltet die Grundinformationen, dort wo [...] vermerkt ist, kommen nun die Inhalte ins Spiel. Ein Blick in das Inhaltsverzeichnis verrät uns, dass die Buchstruktur sehr einfach ist. Wir müssen nur ein paar majorSections hinzufügen:</p>
<!-- /wp:paragraph -->

<!-- wp:code -->
<pre class="wp-block-code"><code>&lt;div type="majorSection" osisID="VORWORT"&gt;
&lt;title&gt;VORWORT&lt;/title&gt;
...
&lt;/div&gt;</code></pre>
<!-- /wp:code -->

<!-- wp:paragraph -->
<p>Die eigentliche Transformation erledigen wir mit Python. Zunächst benötigen wir einige Bibliotheken:</p>
<!-- /wp:paragraph -->

<!-- wp:code -->
<pre class="wp-block-code"><code>import os, subprocess
import re</code></pre>
<!-- /wp:code -->

<!-- wp:paragraph -->
<p>Nun definieren wir noch Ein- und Ausgabe und lesen die Datei:</p>
<!-- /wp:paragraph -->

<!-- wp:code -->
<pre class="wp-block-code"><code>infile = "20200323_KURZE ERKLARUNG DESRDMERBRIEFES_djvu.txt"
outfile = "röm.xml"
f = open(infile, "r")
text = f.read()</code></pre>
<!-- /wp:code -->

<!-- wp:paragraph -->
<p>Jetzt transformieren wir zunächst alle Seitenzahlen in eckige Klammern und entfernen die Zeilenumbrüche:</p>
<!-- /wp:paragraph -->

<!-- wp:code -->
<pre class="wp-block-code"><code>text = re.sub (  r"\n\n\n(&#91;0-9]+)&#91;\s*]\n\n\n",
                  " &#91;\g&lt;1&gt;] "
                    , text)
text = text.replace("  ", "")</code></pre>
<!-- /wp:code -->

<!-- wp:paragraph -->
<p>Das einzige, was uns noch vom direkten Einfügen in unsere Vorlage abhält, sind die Zeilenumbrüche. OSIS verlangt Paragraphen in &lt;p&gt;-Tags. Außerdem haben wir noch im Satz getrennte Wörter. Diese können wir mit der folgenden for-Schleife lösen:</p>
<!-- /wp:paragraph -->

<!-- wp:code -->
<pre class="wp-block-code"><code>lines = text.split("\n")
print (len(lines))
for i in range(0,len(lines)):
    line = lines&#91;i].strip()
    if len(line)&lt;4 and line!="":
        continue
    elif len(line)&lt;20:
        # New Paragraph
        paragraph = paragraph + line
        output = output +  ("&lt;p&gt;"+(paragraph)+"&lt;/p&gt;\n")
        paragraph = ""
    else:
        if line&#91;-1] == "-":
            paragraph = paragraph + line&#91;:-1]
        elif line&#91;-1] == "¬":
            paragraph = paragraph + line&#91;:-1]
        else:
            paragraph = paragraph + line + " "
if paragraph != "":
    output = output +  ("&lt;p&gt;"+(paragraph)+"&lt;/p&gt;\n")</code></pre>
<!-- /wp:code -->

<!-- wp:paragraph -->
<p>Jetzt schreiben wir noch den fertig transformierten Text in die Ausgabedatei und kopieren dann die Inhalte in die Vorlage.</p>
<!-- /wp:paragraph -->

<!-- wp:code -->
<pre class="wp-block-code"><code>f2 = open(outfile, "w")
f2.write(output)</code></pre>
<!-- /wp:code -->

<!-- wp:paragraph -->
<p>Was fangen wir nun mit der XML-Datei an? Wir müssen sie erneut mit den SWORD-Hilfsprogrammen konvertieren:</p>
<!-- /wp:paragraph -->

<!-- wp:code -->
<pre class="wp-block-code"><code>xml2gbs Barth-Röm.xml Barth-Röm</code></pre>
<!-- /wp:code -->

<!-- wp:paragraph -->
<p>Die drei neuen Dateien verschiebt man wieder in das SWORD-Verzeichnis (~/.sword/modules/...) und erzeugt eine conf-Datei, die z.B. so aussehen kann:</p>
<!-- /wp:paragraph -->

<!-- wp:code -->
<pre class="wp-block-code"><code>&#91;Barth-Röm]
DataPath=./modules/genbook/rawgenbook/Barth-Röm/Barth-Röm
ModDrv=RawGenBook
BlockType=CHAPTER
SourceType=OSIS
CompressType=ZIP
GlobalOptionFilter=OSISFootnotes
GlobalOptionFilter=OSISHeadings
Encoding=UTF-8
Lang=de
LCSH=Bible--Commentaries.
Description=Kurze Erklärung des Römerbriefs
About=Kurze Erklärung des Römerbriefs, Karl Barth (1959)
History_1.0=Initial version
DistributionLicense=Public Domain, see https://archive.org/details/20200323kurzeerklarungdesrdmerbriefes_202003/mode/2up</code></pre>
<!-- /wp:code -->

<!-- wp:paragraph -->
<p>Nun kann man den Text von Karl Barth in SWORD bewundern. Wir hätten nur noch ein, zwei Schönheitsoperationen zu erledigen: Die Fußnoten. Zum Glück enthält dieses Buch fast gar keine. Man kann mit einem beliebigen Texteditor nach dem Sternchen * suchen. Die Fußnoten ergänzen wir dann mit dem &lt;note&gt;-Tag:</p>
<!-- /wp:paragraph -->

<!-- wp:code -->
<pre class="wp-block-code"><code>Gestehen wir es gleich zu: Wären uns die Verse 19—21&lt;note type="x-footnote"&gt;* Vgl. zu dieser Stelle KD. I, 2 S. 334 f. und II, 1 S. 131 f.&lt;/note&gt; für sich, vielleicht als Fragment &#91;...]</code></pre>
<!-- /wp:code -->

<!-- wp:paragraph -->
<p>Das automatische Behandeln von Fußnoten ist keine einfache Aufgabe und das händische Nacharbeiten von vielen Fußnoten eine sehr mühsame Fleißarbeit. </p>
<!-- /wp:paragraph -->


<!-- wp:heading -->
<h2>Ulrich Wilckens: <strong>Das Evangelium nach Johannes</strong>  </h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Einige Bücher der Kommentarreihe Neues Testament Deutsch (NTD) stehen bei Digi20 zum Download, z.B. der <a rel="noreferrer noopener" href="https://digi20.digitale-sammlungen.de/de/fs1/object/display/bsb00048509_00001.html" data-type="URL" data-id="https://digi20.digitale-sammlungen.de/de/fs1/object/display/bsb00048509_00001.html" target="_blank">Johannes-Kommentar</a>. Dieses Buch darf nur für private Zwecke digital bearbeitet werden. Andere Bücher (etwa die <a rel="noreferrer noopener" href="https://digi20.digitale-sammlungen.de/de/fs1/object/display/bsb00049180_00005.html" data-type="URL" data-id="https://digi20.digitale-sammlungen.de/de/fs1/object/display/bsb00049180_00005.html" target="_blank">Theologie</a> von Stuhlmacher) stehen wiederum unter <a rel="noreferrer noopener" href="https://de.wikipedia.org/wiki/Creative_Commons" target="_blank">Creative Commons-Lizenz</a> zur Verfügung. Hier müsste jemand mit guten Kenntnissen der rechtlichen Situation die Weitergabe prüfen.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Das <a href="https://de.wikipedia.org/wiki/Texterkennung" target="_blank" rel="noreferrer noopener">OCR</a> des PDFs ist einigermaßen akzeptabel. Allerdings haben wir entweder sehr viel Arbeit mit dem Inhaltsverzeichnis vor uns oder versuchen, automatisch Überschriften und Unterüberschriften zu erkennen. Auch das ist nicht trivial. Schön ist eine durchgängige Form (1. .... 1.1.... 1.1.1.....). Oft werden aber verschiedene Formate verwendet oder noch schlimmer: sogar gewechselt. (§1 Einleitung, 1.1 ... I., II. , XI. ....). Es ist also händisches Nacharbeiten zu erwarten. Immerhin wird in der Reihe NTD nicht mit Fußnoten gearbeitet... </p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Zusätzlich zu unserem Beispiel oben benötigen wir noch eine Bibliothek, die PDF-Dateien lesen kann:</p>
<!-- /wp:paragraph -->

<!-- wp:code -->
<pre class="wp-block-code"><code>import pdfplumber</code></pre>
<!-- /wp:code -->

<!-- wp:paragraph -->
<p>Zuerst definieren wir einige Werte:</p>
<!-- /wp:paragraph -->

<!-- wp:code -->
<pre class="wp-block-code"><code>filename="datei.pdf"  # Eingabedatei
startpage=10 # Die Startseite, ab hier beginnt der Text
endpage= 288 # Letzte Seite 
linelength=45 # Zeilen, die kürzer als 45 Zeichen sind beenden einen Paragraphen</code></pre>
<!-- /wp:code -->

<!-- wp:paragraph -->
<p>Dann lesen wir sukzessive alle entsprechenden Seiten ein:</p>
<!-- /wp:paragraph -->

<!-- wp:code -->
<pre class="wp-block-code"><code>for pagenumber in range(startpage, endpage):    
    page = pdf.pages&#91;pagenumber]
    pagen = page.crop ( (30,0, (page.width), yval) 
    text = pagen.extract_text(x_tolerance=3)</code></pre>
<!-- /wp:code -->

<!-- wp:paragraph -->
<p>Der <code>page.crop</code>-Befehl schneidet uns die Fußzeile mit den Seitenzahlen ab. Ansonsten müssten wir diese nach dem Muster von oben behandeln. Wir trennen den Wert von <code>text</code> nun wie oben nach dem Zeilenende und verarbeiten ihn wie im Beispiel oben weiter:<code> text.split("\n")</code>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Eine kleine Funktion annotiert übrigens auch direkt Links zu Bibelstellen. Dies ist extrem nützlich, um diese nicht händisch nachschlagen zu müssen:</p>
<!-- /wp:paragraph -->

<!-- wp:code -->
<pre class="wp-block-code"><code>def markbooks (text):
    for book in filter:
        text = text.replace (book+"  ", book+" ")
    for book in filter:
        bookn = str(filter&#91;book])
        text = re.sub (   r"("+book.replace(" ", "&#91;\s]")+")&#91;\.]?&#91; ]?(&#91;0-9]+?),&#91; ]?(&#91;0-9]*)(?!-)&#91; ]?.*?(\.?)(&#91;0-9]*)" ,
                       '&lt;reference osisRef="'+str(filter&#91;book])+r'.\g&lt;2&gt;.\g&lt;3&gt;\g&lt;4&gt;'+'"'+r'&gt;\g&lt;0&gt;'+'&lt;/reference&gt;'
                    , text)
        text = re.sub (        r"("+book.replace(" ", "&#91;\s]")+")&#91;\.]?&#91; ]?(&#91;0-9]+?),&#91; ]?(&#91;0-9]*)(-+)&#91; ]?(\.?)(&#91;0-9]*)" ,
                       '&lt;reference osisRef="'+bookn+r'.\g&lt;2&gt;.\g&lt;3&gt;\g&lt;4&gt;'+bookn+r'.\g&lt;2&gt;.\g&lt;6&gt;'+'"'+r'&gt;\g&lt;0&gt;&lt;/reference&gt;'
                    , text)
        # Eine weitere Iteration für Formate wie Röm 3,1; 15.2
        text = re.sub (        r"("+book.replace(" ", "&#91;\s]")+")&#91;\.]?&#91; ]?(&#91;0-9]+?),&#91; ]?(&#91;0-9]*)(?!-)&#91; ]?.*?(\.?)(&#91;0-9]*)&lt;/reference&gt;&#91;;]?&#91;:]?&#91; ]?(&#91;0-9]+?),&#91; ]?(&#91;0-9]*)" ,
                       ''+book+r' \g&lt;2&gt; \g&lt;3&gt;,\g&lt;4&gt;'+'&lt;/reference&gt;'+'; &lt;reference osisRef="'+str(filter&#91;book])+r'.\g&lt;6&gt;.\g&lt;7&gt;'+'"'+r'&gt;\g&lt;6&gt;,\g&lt;7&gt;'+'&lt;/reference&gt;'
                    , text)
    return text</code></pre>
<!-- /wp:code -->

<!-- wp:paragraph -->
<p>Der Inhalt des Dictionaries „filter“ ist dabei reine Fleißarbeit. Hier steht zu jeder möglichen Kurzform eines biblischen Buches ihre enstprechende Normalform:</p>
<!-- /wp:paragraph -->

<!-- wp:code -->
<pre class="wp-block-code"><code>filter = {"1Mo":"Gen","1 Mo":"Gen", "Gen":"Gen", 
          "1Mose":"Gen",
          "2Mo":"Exod","2 Mo":"Exod", "Ex":"Exod", 
          "Exod":"Exod",  "2Mose":"Exod", 
          "3Mo":"Lev","3 Mo":"Lev", "Lev":"Lev", 
          "3Mose":"Lev", 
          "Neh":"Neh", ...</code></pre>
<!-- /wp:code -->